# This is a test file to be converted into lua
=begin
def fun(param)
  param + 5
end
# => s(:defn, :fun, s(:args, :param), s(:call, s(:lvar, :param), :+, s(:lit, 5)))

fun
fun(a)

def fun(arg1, arg2)
  fun
  fun arg1, arg2
end

`--backticks`

"string1"
'string2'

#return "before#{inline}after"

i_am_so_scared = 'of this'

if i_am_so_scared
  puts 'lol'
end

module Test
  class Herp < Derp

  end
end

class Wut < Test::Herp
  def self.stuff

  end
end

Stuff = 'hello'
a = ['a', 'b']
b = { :a => 'b', :c => 'd'}

variable = assignment = chaining = works = somehow = "ugly"
#return nil
def func

end

func('arg1') do |test|
  puts test
end
=end
def puts(text)
  `print(#{text})`
end
puts "THIS IS TOO AWESOME TO BE TRUE"
class TestClass
  def self.test
    puts "WOOT"
  end
end

module TestModule
  def test
    puts "YAY"
  end

  test
end
TestClass.test