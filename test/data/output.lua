--[[Lua generated from Ruby code by Sapphire 0.1.0]]
(function (_sapphire)
    local _scope, self = _sapphire._scope, _sapphire._global
    _scope:defun('puts', function (self, block, ...)
        local _scope = _scope:mkfuncscope(false)
        _scope:make_args({'text'}, {...}, block)
        return print((_scope.lvar:get('text')):call_method('to_s'))
    end)
    _scope.env:call_method('puts', _sapphire.wrap_str("THIS IS TOO AWESOME TO BE TRUE"))
    do
        local _scope = _scope:mkclass('TestClass', nil)
        _scope:defun_static(self, 'test', function (self, block, ...)
            local _scope = _scope:mkfuncscope(false)
            _scope:make_args({''}, {...}, block)
            return _scope.env:call_method('puts', _sapphire.wrap_str("WOOT"))
        end)
    end
    do
        local _scope = _scope:mkmodule('TestModule')
        _scope:defun('test', function (self, block, ...)
            local _scope = _scope:mkfuncscope(false)
            _scope:make_args({''}, {...}, block)
            return _scope.env:call_method('puts', _sapphire.wrap_str("YAY"))
        end)
        _scope.env:call_method('test', nil)

    end
    return _scope:get_const('TestClass'):call_method('test', nil)

end)(Sapphire)