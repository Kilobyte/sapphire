#!/usr/bin/env ruby

require File.join(File.dirname(__FILE__),'../lib/converter')

File.write(ARGV[1], Sapphire::Converter.convert(File.read(ARGV[0])))