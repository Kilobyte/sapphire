function class(name, super)
    local ret = setmetatable({}, {__index = super})
    ret.new = function(...)
        local var = setmetatable({}, {__index = ret})
        var:init()
        return var
    end
    ret.__index = ret
    return ret
end

local Class, Scope, Method, Varlist = class("Class"), class("Scope"), class("Method"), class("Varlist")
local Module = class("Module", Class)
local Object = class("Object")

local NIL, TRUE, FALSE

function Object:init(class)
    self.data = {}
    self.class = class
end

function Class:get_method(name)
    return self.class.scope:getinternal('f_'..name)
end

function Class:call_method(name, ...)
    return self:get_method(name):call(self, nil, ...)
end


function Class:init(name, super, pscope)
    self.name = name
    self.super = super
    self.scope = Scope.new(self, pscope, false, 'class')
end

function Class:get_scope()
    return self.scope
end

function Class:get_method(name)
    return self.scope:getinternal('fs_'..name)
end

function Class:call_method(name, ...)
    local m = self:get_method(name)
    if not m then error("Could not find method "..tostring(self.name).."#"..tostring(name)) end
    m:call(self, nil, ...)
end

function Class:add_method(name, cb)
    self.scope:setinternal('f_'..name, Method.new(cb))
end

function Class:new_inst(...)
    return Object.new(self)
end

function Module:init(name, pscope)
    self.name = name
    self.scope = Scope.new(self, pscope, false, 'module')
end



function Scope:init(env, parent, inherit, type)
    self.data = Varlist.new()
    self.env = env
    self.parent = parent
    self.inherit = inherit
    self.type = type
    self.lvar = Varlist.new(false, inherit and parent.lvar)
end

function Scope:defun(name, callback)
    self.data:set('f_'..name, Method.new(callback))
end

function Scope:defun_static(global, name, callback)
    self.data:set('fs_'..name, Method.new(callback))
end

function Scope:mkfuncscope(name)
    return Scope.new(self.class, self, iter, 'function')
end

function Scope:make_args(names, values, block)
    for a = 1, #names do
        self.lvar:set(names[a], values[a])
    end
    self.block = block
end

function Scope:mkclass(name, super)
    local c = self.data:get('c_'..name)
    if c then return c:get_scope() end
    c = Class.new(name, super, self)
end

function Scope:getglob(name)
    GLOB:get(name)
end

function Scope:get_const(name)
    return self.data:get('c_'..name)
end

function Scope:setglob(name, value)
    GLOB:set(name, value)
end

function Scope:getinternal(name)
    return self.data:get(name)
end

function Scope:setinternal(name, value)
    self.data:set(name, value)
end

function Scope:get_local(name)
    return self.lvar:get(name)
end

function Scope:set_local(name, value)
    self.lvar:set(name, value)
end


function Varlist:init(internal, parent)
    self.internal = internal
    self.data = {}
    self.parent = parent
end

function Varlist:set(name, value)
    if value == NIL then value = nil end
    self.data[name] = value
end

function Varlist:get(name)
    return self.data[name] or ((not self.internal) and NIL or nil)
end


function Method:init(cb)
    self.cb = cb
end

function Method:call(...)
    return self.cb(...)
end

Sapphire = {_env = Module.new('root') }
Sapphire._global = Varlist.new()
Sapphire._scope = Sapphire._env:get_scope()
Sapphire._scope.env = Sapphire._env
Sapphire._scope.global = Sapphire._global

local String = Class.new("String")
String:add_method('initialize', function(self, block, data)
    if type(data) == 'table' and getmetatable(data) == Object then
        self.data = data:call_method('to_s')
    else
        self.data = tostring(data)
    end
end)
String:add_method('to_n', function(self, block)
    return self.data
end)

Sapphire.wrap_str = function(str)
    return String.new_inst(str)
end

local s, m = loadfile("output.lua")
if s then
    setfenv(s, getfenv())
    s()
else
    printError(m)
end