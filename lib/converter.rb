$VERSION = '0.1.0'
require File.join File.dirname(__FILE__), 'generator/lua_generator.rb'
require File.join File.dirname(__FILE__), 'parser/parser.rb'
require File.join File.dirname(__FILE__), 'helper.rb'

module Sapphire
  class Converter
    def self.convert(ruby, min = false)
      Sapphire::LuaGenerator.new(Sapphire::Parser.new.parse(ruby), min).run
    end
  end
end