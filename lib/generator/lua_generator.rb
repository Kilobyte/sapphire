module Sapphire
  class LuaGenerator

=begin
    scopes = [
      :stat, # statement, doesn't need to have a value
      :expr, # expression, HAS to have a value, even if only nil
    ]
=end

    def initialize(sexp, min)
      @indent_current = 0
      @indent = ''
      @indent_lvl = min ? 0 : 4
      @sexp = sexp
      @newline = min ? ' ' : "\n"
      @nl = @newline
    end

    def run
      @scope = Scope.new nil
      puts @sexp.inspect
      tmp = ensure_indent parse_block(@sexp)
      "--[[Lua generated from Ruby code by Sapphire #{$VERSION}]]#{@nl}" +
      "(function (_sapphire)#{@nl}#{' ' * @indent_lvl}local _scope, self = _sapphire._scope, _sapphire._global#{@nl}#{tmp}#{@nl}#{@indent}end)(Sapphire)"
    end

    private

    def ensure_indent(tmp)
      return nil if tmp.nil?
      (tmp.starts_with? @indent) ? (tmp) : (@indent + tmp)
    end

    def unindent
      @indent_current -= @indent_lvl
      @indent = ' ' * @indent_current
      @scope = @scope.parent
      puts "#{@indent}}"
    end

    def indent
      puts "#{@indent}{"
      @indent_current += @indent_lvl
      @indent = ' ' * @indent_current
      @scope = Scope.new @scope
    end

    def parse(sexp, scope)
      return nil if sexp.nil?
      raise Exception.new("Invalid type for s-exp: #{sexp.class}") unless sexp.is_a?(Array)
      puts "#{@indent}parsing #{sexp[0].to_s}"
      send("process_#{sexp[0].to_s}", sexp, scope)
    end

    def parse_checked(sexp, expected, scope)
      raise Exception.new("Expected S-Exp type #{expected.to_s}, got #{sexp[0].to_s}") if sexp[0] != expected
      parse(sexp, scope)
    end

    def process_block(sexp, scope)
      if scope == :expr
        "#{@indent}(function() #{@nl}#{parse_block(sexp)}#{@nl}#{@indent}end)()"
      else
        parse_block(sexp)
      end
    end

    def generate_args(args)
      names = args.join "', '"
      "#{' ' * (@indent_current + @indent_lvl)}_scope:make_args({'#{names}'}, {...}, block)#{@nl}"
    end

    def make_fun(name, args, code, iter = false)
      #name = "f_#{name}"

      c = "#{' ' * (@indent_current + @indent_lvl)}local _scope = _scope:mkfuncscope(#{iter})#{@nl}"
      c += generate_args args[1...args.length]
      c += parse_block(code ||= [:block])
      c += "#{@newline}" unless c.ends_with? "#{@newline}"

      {:name => name, :code => c}
    end

    def process_defn(sexp, scope)
      fun = make_fun(sexp[1], sexp[2], sexp[3])
      "_scope:defun('#{fun[:name]}', function (self, block, ...)#{@nl}#{fun[:code]}#{@indent}end)"
    end

    def process_defs(sexp, scope)
      fun = make_fun(sexp[2], sexp[3], sexp[4])
      clazz = parse(sexp[1], :expr)
      "_scope:defun_static(#{clazz}, '#{fun[:name]}', function (self, block, ...)#{@nl}#{fun[:code]}#{@indent}end)"
    end

    def process_args(sexp, scope)
      sexp[1...sexp.length].collect { |el|
        "a_#{el}"
      }.join ', '
    end

    def process_lit(sexp, scope)
      "_sapphire.wrap(#{sexp[1]})"
    end

    def process_lvar(sexp, scope)
      name = sexp[1]
      #if name == 'set' || name == 'get'
        "_scope.lvar:get('#{name}')"
      #else
      #  "_scope.lvar['#{name}']"
      #end
    end

    def process_call(sexp, scope)
      obj = (parse sexp[1], :expr) || '_scope.env'
      name = sexp[2].to_s
      args = sexp[3...sexp.length].collect! do |el|
        parse el, :expr
      end
      "#{obj}:call_method('#{name.gsub '\'', '\\\''}', #{args.length == 0 ? 'nil' : args.join(', ')})"

    end

    def process_xstr(sexp, scope)
      sexp[1]
    end

    def process_str(sexp, scope)
      to_lua_string sexp[1]
    end

    def to_lua_string(stuff)
      "_sapphire.wrap_str(\"#{stuff.gsub("\n", '\\n').gsub('\"', '\\\"')}\")"
    end

    def process_return(sexp, scope)
      "return #{parse(sexp[1], :expr)}"
    end

    def process_implreturn(sexp, scope)
      sexp_ = sexp[1][0] == :return ? sexp[1] : sexp
      "return #{parse(sexp_[1], :expr)}"
    end

    def process_dstr(sexp, scope)
      sexp[1...sexp.length].collect { |el|
        if el.is_a? String
          to_lua_string el
        else
          parse el, :expr
        end
      }.join '..'
    end

    def process_dxstr(sexp, scope)
      sexp[1...sexp.length].collect { |el|
        ret = el if el.is_a? String
        ret = el[1] if el.is_a? Array and el[0] == :str
        ret = parse el, :expr unless ret
        ret
      }.join ''
    end

    def process_evstr(sexp, scope)
      "(#{parse(sexp[1], :expr)}):call_method('to_s')"
    end

    def process_lasgn(sexp, scope)
      name = sexp[1]
      val = parse sexp[2], :expr
      #if scope == :expr || name == 'set' || name == 'get'
        "_scope:set_local('#{name}', #{val})"
      #else
      #  "_scope.lvar['#{name}'] = #{val}"
      #end
    end

    def process_if(sexp, scope)
      c = parse sexp[1], :expr
      t = ensure_indent(parse_block(sexp[2] || [:block]))
      e = ensure_indent(parse_block(sexp[3] || [:block]))
      "#{scope == :expr ? '(function() ' : ''}if #{c} then#{@newline}#{t}#{@newline}#{@indent}else#{@newline}#{e}#{@newline}#{@indent}end#{scope == :expr ? ' end)()' : ''}"
    end

    def parse_block(sexp, retvar = nil, scope = :stat)
      sexp = flatn_block sexp
      s = sexp
      if retvar and retvar != ''
        if s.length == 0
          s[0] = [:retvar, retvar, [:nil]]
        else
          s[s.length - 1] = [:retvar, retvar, s[s.length - 1]]
        end
      elsif retvar.nil?
        if s.length == 0
          s[0] = [:implreturn, [:nil]]
        else
          s[s.length - 1] = [:implreturn, s[s.length - 1]]
        end
      end

      code = ''
      indent
      s.each do |el|
        code += "#{@indent}#{parse(el, scope)}#{@newline}"
      end
      code = ensure_indent code
      unindent
      code
    end

    def parse_retvar(sexp, scope)
      name = sexp[1]
      val = parse(sexp[2], :expr)
      "#{name} = #{val}"
    end

    def process_nil(sexp, scope)
      'nil'
    end

    def flatn_block(sexp)
      return [sexp] if sexp[0] != :block
      ret = []
      sexp[1...sexp.length].each { |el|
        flatn_block(el).each { |el2|
          ret << el2
        }
      }
      ret
    end

    def wrap_sexp(exp, type)
      exp.unshift type
    end

    def process_module(sexp, scope)
      name = sexp[1]
      code = ensure_indent(parse_block(wrap_sexp(sexp[2...sexp.length], :block), ''))
      i = ' ' * (@indent_current + @indent_lvl)
      "do#{@nl}#{i}local _scope = _scope:mkmodule('#{name}')#{@nl}#{code}#{@nl}#{@indent}end"
    end

    def process_self(sexp, scope)
      'self'
    end

    def process_class(sexp, scope)
      name = sexp[1]
      sup = parse(sexp[2], :expr) || 'nil'
      indent
      code = ensure_indent(parse sexp[3], :stat)
      i = @indent
      unindent
      "do#{@nl}#{i}local _scope = _scope:mkclass('#{name}', #{sup})#{@nl}#{code}#{@nl}#{@indent}end"
    end

    def process_const(sexp, scope)
      "_scope:get_const('#{sexp[1]}')"
    end

    def process_colon2(sexp, scope)
      var = parse sexp[1], :expr
      "#{var}:colon2(0, '#{sexp[2]}')"
    end

    def process_super(sexp, scope)
      'self._super'
    end

    def process_cdecl(sexp, scope)
      "_scope:set_const('#{sexp[1]}', #{parse(sexp[2], :expr)})"
    end

    def process_array(sexp, scope)
      "_sapphire.wrap_array({#{(sexp[1...sexp.length].collect {|el|
        parse el, :expr
      }.join ', ')}})"
    end

    def process_hash(sexp, scope)
      ret = []
      key = nil
      sexp[1...sexp.length].each {|el|
        if key
          ret << "[#{parse key, :expr}] = #{parse el, :expr}"
          key = nil
        else
          key = el
        end
      }
      "_sapphire.wrap_hash({#{ret.join ', '}})"
    end

    def process_iter(sexp, scope)
      f = sexp[1]#parse_block(sexp[1], nil, :block)

      obj = (parse f[1], :expr) || '_scope.env'
      name = "f_#{f[2]}"
      args = f[3...f.length].collect! do |el|
        parse el, :expr
      end

      fref = sexp[3]
      fun = make_fun('', sexp[2] || [:args], sexp[3], true)

      func = "function(self, block, ...)#{@nl}#{fun[:code]}#{@nl}#{@indent}end"

      "#{obj}['#{name.gsub '\'', '\\\''}']( #{([obj, func] + args).join ', '})"
    end

  end

  class Scope

    attr_reader :parent

    def initialize(parent)
      @parent = parent
      @locals = {}
    end

    def define_local(name)
      @locals[name.to_sym] = true
    end

    def has_local?(name)
      @locals[name.to_sym] || false
    end
  end
end