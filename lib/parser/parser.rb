require 'ruby_parser'

module Sapphire
  class Parser
    def initialize
      @parser = RubyParser.new
    end

    def parse(ruby)
      @parser.parse ruby
    end
  end
end