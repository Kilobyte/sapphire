class String
  def ends_with?(str)
    str = str.to_str
    tail = self[-str.length, str.length]
    tail == str
  end

  def starts_with?(str)
    str = str.to_str
    head = self[0, str.length]
    head == str
  end
end